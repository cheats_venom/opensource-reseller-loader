#include "Log.h"
#include <string.h>
#include <cstdarg>
#include <stdio.h>
#include <fstream>
#include <windows.h>
#include <time.h>
#include <iostream>
#include "fileutils/fileutils.h"
#include <Va_string/Variadicstring.h>
#include <Version/Version.h>

const std::string CurrentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	localtime_s(&tstruct, &now);
	strftime(buf, sizeof(buf), "%d/%m/%Y %X", &tstruct);

	return buf;
}
void Log::Log(const char* fmt, ...)
{
	if (!fmt)	return;
	char text[4096];
	char log[4096];
	va_list		ap;
	va_start(ap, fmt);
	std::string date = "[ " + CurrentDateTime() + "]";
	vsprintf_s(text, fmt, ap);
	strcpy_s(log, date.c_str());
	strcat_s(log, text);
	va_end(ap);

	auto DefaultDir = fileutils::GetDir();
	std::ofstream logfile(va_string(("%s\\Logs\\Loader.log"), DefaultDir), std::ios::app);
	if (logfile.is_open() && log)	logfile << log << std::endl;
	logfile.close();
}