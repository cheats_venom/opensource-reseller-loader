#pragma once
#include <string>
#include <vector>
#include <Singleton.h>
#include <cpprest\http_client.h>
#include <cpprest\filestream.h>
#include <cpprest/json.h>                     
#include <cpprest/uri.h>              
#include <cpprest/containerstream.h>            // Async streams backed by STL containers
#include <cpprest/interopstream.h>              // Bridges for integrating Async streams with STL and WinRT streams
#include <cpprest/rawptrstream.h>               // Async streams backed by raw pointer to memory
#include <cpprest/producerconsumerstream.h>     // Async streams for producer consumer scenarios

class WebRequest
	: public Singleton<WebRequest>
{
	friend class Singleton<WebRequest>;
	WebRequest(void)
	{
	}

	WebRequest(WebRequest const&) = default;

	WebRequest(WebRequest&&) = default;

	WebRequest& operator=(WebRequest const&) = default;

	WebRequest& operator=(WebRequest&&) = default;

	~WebRequest(void)
	{
	}
public:
	struct WebParameters {
		std::string name;
		std::string result;
	};
	
	std::string GetRequest(std::string);
	std::wstring GetWideRequest(std::string);
	bool Download(std::string, std::string);
	std::string PostJson(std::string, web::json::value data);
};