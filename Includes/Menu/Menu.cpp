#include "Menu.h"
#include <d3d11.h>
#include <dwmapi.h>
#include <thread>
#include <iostream>
#include "imgui/imgui_impl_win32.h"
#include "imgui/imgui_impl_dx11.h"
#include "Menu/imgui/imgui_internal.h"
#include "LoaderThreads/LoaderThreads.h"
#include "Server/Server.h"
#include "fileutils/fileutils.h"
#define STB_IMAGE_IMPLEMENTATION
#include "Menu/imgui/stb_image.h"
#include "Web/WebRequest.h"
#include "Va_string/Variadicstring.h"

ID3D11Device* g_pd3dDevice = NULL;
ID3D11DeviceContext* g_pd3dDeviceContext = NULL;
IDXGISwapChain* g_pSwapChain = NULL;
ID3D11RenderTargetView* g_mainRenderTargetView = NULL;
HWND hwnd = NULL;
bool CreateDeviceD3D(HWND hWnd);
void CleanupDeviceD3D();
void CreateRenderTarget();
void CleanupRenderTarget();
bool LoadTextureFromMemory(unsigned char* Buffer, size_t Size, ID3D11ShaderResourceView** out_srv, int* out_width, int* out_height);
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
ImFont* BIGBIG;
ImFont* MEDIUM;
ID3D11ShaderResourceView* Logo;
int LogoX;
int LogoY;

int my_image_width = 0;
int my_image_height = 0;
ID3D11ShaderResourceView* my_texture = NULL;


bool LoadTextureFromFile(const char* filename, ID3D11ShaderResourceView** out_srv, int* out_width, int* out_height)
{
    // Load from disk into a raw RGBA buffer
    int image_width = 0;
    int image_height = 0;
    unsigned char* image_data = stbi_load(filename, &image_width, &image_height, NULL, 4);
    if (image_data == NULL)
        return false;

    // Create texture
    D3D11_TEXTURE2D_DESC desc;
    ZeroMemory(&desc, sizeof(desc));
    desc.Width = image_width;
    desc.Height = image_height;
    desc.MipLevels = 1;
    desc.ArraySize = 1;
    desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc.SampleDesc.Count = 1;
    desc.Usage = D3D11_USAGE_DEFAULT;
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    desc.CPUAccessFlags = 0;

    ID3D11Texture2D* pTexture = NULL;
    D3D11_SUBRESOURCE_DATA subResource;
    subResource.pSysMem = image_data;
    subResource.SysMemPitch = desc.Width * 4;
    subResource.SysMemSlicePitch = 0;
    g_pd3dDevice->CreateTexture2D(&desc, &subResource, &pTexture);

    // Create texture view
    D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
    ZeroMemory(&srvDesc, sizeof(srvDesc));
    srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srvDesc.Texture2D.MipLevels = desc.MipLevels;
    srvDesc.Texture2D.MostDetailedMip = 0;
    g_pd3dDevice->CreateShaderResourceView(pTexture, &srvDesc, out_srv);
    pTexture->Release();

    *out_width = image_width;
    *out_height = image_height;
    stbi_image_free(image_data);

    return true;
}

RECT center_window(HWND parent_window, int width, int height)
{
    RECT rect;
    GetClientRect(parent_window, &rect);
    rect.left = (rect.right / 2) - (width / 2);
    rect.top = (rect.bottom / 2) - (height / 2);
    return rect;
}
WNDCLASSEX wc;


bool tryed = false;
void LoadLogo(std::string url) {
    tryed = true;
    bool down = WebRequest::Get().Download(url, "C:\\str.png");
    if (down) {
        bool ret = LoadTextureFromFile("C:\\str.png", &my_texture, &my_image_width, &my_image_height);
        if (!ret) {
            my_texture = NULL;
        }
    }
}

bool  Menu::Start() {
    wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, "LOADER__", NULL };
    wc.hInstance = instance;
    ::RegisterClassEx(&wc);
    int width = 500, height = 425;
    RECT rect;
    GetClientRect(GetDesktopWindow(), &rect);
    auto center = center_window(GetDesktopWindow(), width, height);
    hwnd = CreateWindowExA(0, wc.lpszClassName, fileutils::RandomString(8).c_str(), WS_POPUP | WS_VISIBLE, center.left, center.top, width, height, NULL, NULL, instance, NULL);
    if (!CreateDeviceD3D(hwnd))
    {
        CleanupDeviceD3D();
        ::UnregisterClass(wc.lpszClassName, wc.hInstance);
        return false;
    }
    MARGINS Margin = { -1 };
    DwmExtendFrameIntoClientArea(hwnd, &Margin);
    ::ShowWindow(hwnd, SW_SHOWDEFAULT);
    ::UpdateWindow(hwnd);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\verdanab.ttf", 16.000f);
    BIGBIG = io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\Verdanab.ttf", 34.000f);
    MEDIUM = io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\Verdanab.ttf", 18.000f);

    ImGui::StyleColorsDark();

    ImGui_ImplWin32_Init(hwnd);
    ImGui_ImplDX11_Init(g_pd3dDevice, g_pd3dDeviceContext);

    return true;
}


void LoginKey(std::string key) {
    Server::Get().LoginKey(key);
}
void InjectKey(std::string key, int cheat_id) {
    Server::Get().InjectKey(key, cheat_id);
}



void DrawTextCentered(const char* text)
{
    ImGui::SetCursorPosX((ImGui::GetWindowWidth() - ImGui::CalcTextSize(text).x) / 2.f);
    ImGui::Text(text);
}

void Menu::Loop() {
        bool done = true;
    bool firstTime = true;
    bool configureSpoofer = false;
    int selectedCheat = 0;
    std::vector<const char*> combo_list;
    while (done)
    {
        MSG msg;
        while (::PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
        {
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
            if (msg.message == WM_QUIT)
                done = false;
        }

        if (!done)
            break;

        // Start the Dear ImGui frame
        ImGui_ImplDX11_NewFrame();
        ImGui_ImplWin32_NewFrame();
        ImGui::NewFrame();
        if (firstTime) {
            ImGui::SetNextWindowPos({ 0, 0 });
            firstTime = false;
        }

        if (combo_list.size() != available_cheats.size()) {
            for (const auto& cheat : available_cheats) {
                combo_list.push_back(cheat.name.c_str());
            }
        }


        ImGuiStyle& style = ImGui::GetStyle();
        style.Colors[ImGuiCol_WindowBg] = ImColor(24, 26, 23, 255);
        style.Colors[ImGuiCol_Button] = ImColor(253, 78, 57, 255);
        style.Colors[ImGuiCol_ButtonActive] = ImColor(253, 78, 57, 255);
        style.Colors[ImGuiCol_ButtonHovered] = ImColor(184, 56, 41, 255);
        ImGui::SetNextWindowSize(ImVec2(500, 425)); // format = width height
        if (ImGui::Begin(("Loader"), &done, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar))
        {
            style.FrameRounding = 2.f;
            ImGui::SetCursorPos(ImVec2(470, 5));
            if (ImGui::Button("X", { 25, 30 }))
            {
                done = false;
                fileutils::TerminateLoader();
            }
            if (page == 0) {
                ImGui::PushFont(MEDIUM);
                style.Colors[ImGuiCol_Text] = ImColor(25, 216, 22, 255);
                if (isError) {
                    style.Colors[ImGuiCol_Text] = ImColor(216, 22, 22, 255);
                }
                if (message.c_str()) {
                    ImGui::SetCursorPos(ImVec2(0.000f, is_team ? 40.000f : 130.000f));
                    DrawTextCentered(message.c_str());
                }
                ImGui::PopFont();
                ImGui::PushFont(MEDIUM);
                style.FramePadding = ImVec2(5, 15);
                style.FrameRounding = 10.f;
                style.FrameBorderSize = 1.f;
                style.Colors[ImGuiCol_Text] = ImColor(255, 255, 255, 255);
                style.Colors[ImGuiCol_Border] = ImColor(0, 0, 0, 255);
                auto oldFrame = style.Colors[ImGuiCol_FrameBg];
                style.Colors[ImGuiCol_FrameBg] = ImColor(33, 36, 32, 255);
                {
                    ImGui::SetNextItemWidth(450.000f);
                    ImGui::SetCursorPos(ImVec2(20.000f, 161.383f));
                    ImGui::InputTextCPP(("##Key"), &key);
                }

                style.FramePadding = ImVec2(0, 5);
                style.FrameRounding = 2.f;
                ImGui::SetCursorPos(ImVec2(20.000f, is_team ? 281.000f : 281.000f));
                if (Loading)
                    ImGui::BeginDisabled();
                if (ImGui::Button(Loading ? ("LOGGING...") : ("LOGIN"), ImVec2(450.000f, 50.000f))) {
                    message = "";
                    time_left = "";
                    cheat_name = "";
                    if (key.size() < 15) {
                        isError = true;
                        Loading = false;
                        message = std::string(("Invalid key"));
                    }
                    else {
                        std::thread(LoginKey, key).detach();
                    }
                }
                if (Loading)
                    ImGui::EndDisabled();
                ImGui::PopFont();


                ImGui::PushFont(BIGBIG);
                ImGui::SetCursorPos(ImVec2(0.000f, 331.000f));
                DrawTextCentered(("PUT YOUR KEY TO CONTINUE"));
                ImGui::PopFont();
            }
            else  if (page == 1) {
                ImGui::PushFont(MEDIUM);

                if (is_team) {
                    ImGui::SetCursorPos(ImVec2(30.000f, 170.f));
                    ImGui::Text("Select the cheat:");
                    ImGui::SetNextItemWidth(450.000f);
                    ImGui::SetCursorPos(ImVec2(30.000f, 190.f));
                    if (ImGui::Combo("##cheat", &selectedCheat, combo_list.data(), combo_list.size())) {
                        auto cheat = available_cheats[selectedCheat];
                        hasSpoofer = cheat.hasSpofer;
                        if (cheat.active) {
                            isError = false;
                            message = "";
                        }
                        else {
                            isError = true;
                            message = std::string(("Cheat is under maintence"));
                        }
                    }
                }
                else {
                    if (!time_left.empty()) {
                        ImGui::SetCursorPos(ImVec2(20.000f, 170.f));
                        DrawTextCentered(time_left.c_str());

                    }
                    if (!cheat_name.empty()) {
                        ImGui::PushFont(BIGBIG);
                        ImGui::SetCursorPos(ImVec2(0.000f, 190.f));
                        DrawTextCentered(cheat_name.c_str());
                        ImGui::PopFont();
                    }
                }

                style.Colors[ImGuiCol_Text] = ImColor(25, 216, 22, 255);
                if (isError) {
                    style.Colors[ImGuiCol_Text] = ImColor(216, 22, 22, 255);
                }

                if (message.c_str()) {
                    ImGui::SetCursorPos(ImVec2(0.000f, 260.383f));
                    DrawTextCentered(message.c_str());
                }
                ImGui::PopFont();

                if (!store_name.empty()) {
                    ImGui::PushFont(BIGBIG);
                    style.Colors[ImGuiCol_Text] = ImColor(255, 255, 255, 255);
                    ImGui::SetCursorPos(ImVec2(0.000f, 40.383f));
                    DrawTextCentered(store_name.c_str());
                    ImGui::PopFont();
                }

                if (!store_logo.empty()) {
                    if (!my_texture && !tryed) {
                        LoadLogo(store_logo);
                    }

                    if (my_texture) {
                        ImGui::SetCursorPos(ImVec2(205.000f, 80.383f));
                        ImGuiWindow* window = ImGui::GetCurrentWindow();
                        ImRect bb(window->DC.CursorPos, ImVec2(window->DC.CursorPos.x + 90, window->DC.CursorPos.y + 90));
                        ImGui::ItemSize(bb);
                        if (!ImGui::ItemAdd(bb, 0)) {
                            return;
                        }
                        window->DrawList->AddImageRounded(my_texture, bb.Min, bb.Max, ImVec2(0, 0), ImVec2(1, 1), ImGui::GetColorU32(ImVec4(1, 1, 1, 1)), 90.f);
                        //ImGui::Image((void*)my_texture, ImVec2(90, 90));
                    }
                }

                ImGui::PushFont(MEDIUM);
                style.FramePadding = ImVec2(5, 15);
                style.FrameRounding = 10.f;
                style.FrameBorderSize = 1.f;
                style.Colors[ImGuiCol_Text] = ImColor(255, 255, 255, 255);
                style.Colors[ImGuiCol_Border] = ImColor(0, 0, 0, 255);
                style.FramePadding = ImVec2(0, 5);
                style.FrameRounding = 2.f;
                if (hasSpoofer) {
                    ImGui::SetCursorPos(ImVec2(25.000f, 230.000f));
                    if (ImGui::Checkbox(("Enable Spoofer"), &Server::Get().wantSpoofer)) {
                        Server::Get().Save();
                    }
                    ImGui::SameLine();
                    if (ImGui::Button(("Configure Spoofer"), ImVec2(170.000f, 30.000f))) {
                        configureSpoofer = !configureSpoofer;
                    }
                }
                ImGui::SetCursorPos(ImVec2(20.000f, 296.000f));
                if (Loading || isError)
                    ImGui::BeginDisabled();
                if (ImGui::Button(Loading ? ("INJECTING...") : ("INJECT"), ImVec2(450.000f, 50.000f))) {
                    Loading = true;
                    message = "";

                    if (key.size() < 15) {
                        isError = true;
                        Loading = false;
                        message = std::string(("Invalid key"));
                    }
                    else {
                        if (is_team) {
                            std::thread(InjectKey, key, available_cheats[selectedCheat].id).detach();
                        }
                        else {
                            std::thread(InjectKey, key, 0).detach();
                        }
                    }

                }
                if (Loading || isError)
                    ImGui::EndDisabled();
                ImGui::PopFont();
            }
            ImGui::End();
            if (configureSpoofer) {
                ImGui::SetNextWindowPos({ 125, 15 });
                ImGui::SetNextWindowSize(ImVec2(250, 320)); // format = width height
                if (ImGui::Begin(("Spoofer"), &done, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoTitleBar))
                {
                    style.FrameRounding = 2.f;
                    ImGui::SetCursorPos(ImVec2(210, 10));
                    if (ImGui::Button("X", { 25, 30 }))
                    {
                        configureSpoofer = !configureSpoofer;
                    }

                    if (ImGui::Checkbox(("Spoof Disks"), &Server::Get().spoofDisk)) {
                        Server::Get().Save();
                    }
                    if (ImGui::Checkbox(("Null Disk"), &Server::Get().spoofDiskType)) {
                        Server::Get().Save();
                    }
                    if (ImGui::Checkbox(("Spoof MOBO"), &Server::Get().spoofMobo)) {
                        Server::Get().Save();
                    }
                    if (ImGui::Checkbox(("Spoof Boot"), &Server::Get().spoofBoot)) {
                        Server::Get().Save();
                    }
                    if (ImGui::Checkbox(("Spoof MAC"), &Server::Get().spoofMAC)) {
                        Server::Get().Save();
                    }
                    if (ImGui::Checkbox(("Spoof Monitor"), &Server::Get().spoofMonitor)) {
                        Server::Get().Save();
                    }
                    if (ImGui::Checkbox(("Spoof GPU"), &Server::Get().spoofGPU)) {
                        Server::Get().Save();
                    }
                    if (ImGui::Checkbox(("Spoof TPM"), &Server::Get().spoofTPM)) {
                        Server::Get().Save();
                    }
                    if (ImGui::Checkbox(("Spoof Registry"), &Server::Get().spoofFileRegistry)) {
                        Server::Get().Save();
                    }

                    if (ImGui::Checkbox(("Use Static Serial"), &Server::Get().spoofStaticSerial)) {
                        Server::Get().Save();
                    }
                    if (Server::Get().spoofStaticSerial) {
                        ImGui::Checkbox(("Reset Static Serial Seed"), &Server::Get().spoofStaticSerialRandomSeed);
                    }

                }
                ImGui::End();
            }
        }
        // Rendering
        ImGui::Render();
        const float clear_color_with_alpha[4] = { 0.f, 0.f, 0.f, 0.f };
        g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, NULL);
        g_pd3dDeviceContext->ClearRenderTargetView(g_mainRenderTargetView, clear_color_with_alpha);
        ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

        g_pSwapChain->Present(1, 0); // Present with vsync
    }
    ImGui_ImplDX11_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();

    CleanupDeviceD3D();
    ::DestroyWindow(hwnd);
    ::UnregisterClass(wc.lpszClassName, wc.hInstance);
}


bool CreateDeviceD3D(HWND hWnd)
{
    // Setup swap chain
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 2;
    sd.BufferDesc.Width = 0;
    sd.BufferDesc.Height = 0;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;
    sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    UINT createDeviceFlags = 0;
    //createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
    D3D_FEATURE_LEVEL featureLevel;
    const D3D_FEATURE_LEVEL featureLevelArray[2] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0, };
    if (D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevelArray, 2, D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &featureLevel, &g_pd3dDeviceContext) != S_OK)
        return false;

    CreateRenderTarget();
    return true;
}

void CleanupDeviceD3D()
{
    CleanupRenderTarget();
    if (g_pSwapChain) { g_pSwapChain->Release(); g_pSwapChain = NULL; }
    if (g_pd3dDeviceContext) { g_pd3dDeviceContext->Release(); g_pd3dDeviceContext = NULL; }
    if (g_pd3dDevice) { g_pd3dDevice->Release(); g_pd3dDevice = NULL; }
}

void CreateRenderTarget()
{
    ID3D11Texture2D* pBackBuffer;
    g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
    g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_mainRenderTargetView);
    pBackBuffer->Release();
}

void CleanupRenderTarget()
{
    if (g_mainRenderTargetView) { g_mainRenderTargetView->Release(); g_mainRenderTargetView = NULL; }
}

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

// Win32 message handler
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
        return true;

    switch (msg)
    {
    case WM_SIZE:
        if (g_pd3dDevice != NULL && wParam != SIZE_MINIMIZED)
        {
            CleanupRenderTarget();
            g_pSwapChain->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
            CreateRenderTarget();
        }
        return 0;
    case WM_SYSCOMMAND:
        if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
            return 0;
        break;
    case WM_DESTROY:
        ::PostQuitMessage(0);
        return 0;
    }
    return ::DefWindowProc(hWnd, msg, wParam, lParam);
}


bool LoadTextureFromMemory(unsigned char* Buffer, size_t Size, ID3D11ShaderResourceView** out_srv, int* out_width, int* out_height)
{
    // Load from disk into a raw RGBA buffer
    int image_width = 0;
    int image_height = 0;
    unsigned char* image_data = stbi_load_from_memory(Buffer, Size, &image_width, &image_height, NULL, 4);
    if (image_data == NULL)
        return false;

    // Create texture
    D3D11_TEXTURE2D_DESC desc;
    ZeroMemory(&desc, sizeof(desc));
    desc.Width = image_width;
    desc.Height = image_height;
    desc.MipLevels = 1;
    desc.ArraySize = 1;
    desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc.SampleDesc.Count = 1;
    desc.Usage = D3D11_USAGE_DEFAULT;
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    desc.CPUAccessFlags = 0;

    ID3D11Texture2D* pTexture = NULL;
    D3D11_SUBRESOURCE_DATA subResource;
    subResource.pSysMem = image_data;
    subResource.SysMemPitch = desc.Width * 4;
    subResource.SysMemSlicePitch = 0;
    g_pd3dDevice->CreateTexture2D(&desc, &subResource, &pTexture);

    // Create texture view
    D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
    ZeroMemory(&srvDesc, sizeof(srvDesc));
    srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srvDesc.Texture2D.MipLevels = desc.MipLevels;
    srvDesc.Texture2D.MostDetailedMip = 0;
    g_pd3dDevice->CreateShaderResourceView(pTexture, &srvDesc, out_srv);
    pTexture->Release();

    *out_width = image_width;
    *out_height = image_height;
    stbi_image_free(image_data);

    return true;
}