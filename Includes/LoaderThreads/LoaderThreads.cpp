#include "LoaderThreads.h"
#include <mutex>
#include <thread>
#include <iostream>
#include <document.h>
#include <writer.h>
#include "fileutils/fileutils.h"
#include "NotifyPopup/NotifyIconPop.h"
#include "Language/Language.h"
#include "Version/Version.h"
#include "Requirements/Requirements.h"
#include "Log/Log.h"
#include "Web/WebRequest.h"
#include "Menu/Menu.h"
#include "Server/Server.h"

using namespace std::chrono_literals; 

std::mutex g_varMutex;

HANDLE ThreadIDCustom = 0;
HANDLE ThreadIDEntry = 0;

bool run_threads = true;

VOID EntryPoint() {
	if (!Version::Check()) {
		fileutils::TerminateLoader();
	}

	if (!Requirements::CheckRequirements()) {
		fileutils::TerminateLoader();
	}

	Menu::Get().key.reserve(255);
	Server::Get().Load();
	Menu::Get().Start();
	Menu::Get().Loop();


	CloseHandle(GetCurrentThread()); //close the current thread
}

void LoaderThreads::Start() {
	EntryPoint();
}

void LoaderThreads::ErrorClose(const char* Message){
	Menu::Get().message = Message;
	Menu::Get().Loading = true;
	Menu::Get().isError = true;
	Sleep(5000);
	fileutils::TerminateLoader();
}

void LoaderThreads::SuccessClose(const char* Message) {
	Menu::Get().message = Message;
	Menu::Get().Loading = true;
	Menu::Get().isError = false;
	Sleep(10000);
	fileutils::TerminateLoader();
}