#include "Requirements.h"
#include <iostream>
#include <string>
#include <fstream>
#include <thread>
#include <vector>
#include <netfw.h>
#include <map>
#include <chrono>
#include <tbs.h>
#include "fileutils/fileutils.h"
#include "Web/WebRequest.h"
#include "Va_string/Variadicstring.h"
#include "NotifyPopup/NotifyIconPop.h"
#include "Log/Log.h"
#include "Version/Version.h"
#include "PortableExecutable/PortableExecutable.h"

bool IsDefenderEnabled() {

    TCHAR szFilename[1024];
    ExpandEnvironmentStrings("%ProgramFiles%\\Windows Defender\\\MpClient.dll", szFilename, (sizeof(szFilename) / sizeof(szFilename[0])));
    HMODULE hMod = LoadLibraryA(szFilename);
    if (hMod == NULL)
        return false;

    typedef HRESULT(WINAPI* PWDStatus)(__out BOOL* pfEnabled);
    PWDStatus pWDStatus = (PWDStatus)GetExportAddress(hMod, "WDStatus", TRUE);
    if (pWDStatus == NULL) {
        FreeLibrary(hMod);
        return false;
    }

    BOOL status;
    HRESULT hr = pWDStatus(&status);
    FreeLibrary(hMod);

    return status;

}

bool CheckDefender() {
    if (IsDefenderEnabled())
    {
        fileutils::MessageBox(NULL, "Windows Defender is turned on. Download Defender Control and use it to disable.", "Info", MB_SYSTEMMODAL | MB_OK | MB_ICONERROR, NULL, 20000);
        return false;
    }
    return true;
}

bool IsLegacyEnabled() {
    FIRMWARE_TYPE fwtype;
    DWORD dwRet;
    GetFirmwareType(&fwtype);
    if (fwtype != FirmwareTypeUefi)
        return false;
    if (!fileutils::AdjustCurrentPrivilege(SE_SYSTEM_ENVIRONMENT_NAME))
        return false;
    dwRet = 0;
    if (GetFirmwareEnvironmentVariableA((""), ("{00000000-0000-0000-0000-000000000000}"), NULL, 0) == 0) {
        if (GetLastError() == ERROR_INVALID_FUNCTION) {
            return true;
        }
        if (GetLastError() == ERROR_NOACCESS) {
            return false;
        }
        else {
            return false;
        }
        return GetLastError() != ERROR_NOACCESS;
    }
    return false;
}

bool CheckUEFI() {
    if (IsLegacyEnabled())
    {
        fileutils::MessageBox(NULL, ("Your BIOS mode must be UEFI."), ("Info"), MB_SYSTEMMODAL | MB_OK | MB_ICONERROR, NULL, 20000);
        return false;
    }
    return true;
}

bool Requirements::CheckRequirements() {
    if (!CheckDefender())
        return false;
    if (!CheckUEFI())
        return false;
    return true;
}
