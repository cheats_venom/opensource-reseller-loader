#pragma once
#include <windows.h>
#include <string>
#include <vector>
#include <Singleton.h>
#include <Menu/Menu.h>

class Server
	: public Singleton<Server>
{
	friend class Singleton<Server>;
	Server(void)
	{
	}

	Server(Server const&) = default;

	Server(Server&&) = default;

	Server& operator=(Server const&) = default;

	Server& operator=(Server&&) = default;

	~Server(void)
	{
	}
public:
	void LoginKey(std::string key);
	bool LoginTeam(std::string key);
	void InjectKey(std::string key, int cheat_id);
	void Load();
	void Save();
	bool wantSpoofer = true;
	bool spoofDisk = true;
	bool spoofDiskType = false;
	bool spoofMobo = true;
	bool spoofBoot = true;
	bool spoofMAC = true;
	bool spoofMonitor = true;
	bool spoofGPU = true;
	bool spoofTPM = true;
	bool spoofFileRegistry = true;

	bool spoofStaticSerial = false;
	bool spoofStaticSerialRandomSeed = false;

};
