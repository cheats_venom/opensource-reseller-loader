#include <windows.h>
#include <Singleton.h>
#include <string>


class NotifyIconPopup
	: public Singleton<NotifyIconPopup>
{
	friend class Singleton<NotifyIconPopup>;
	NotifyIconPopup(void)
	{
	}

	NotifyIconPopup(NotifyIconPopup const&) = default;

	NotifyIconPopup(NotifyIconPopup&&) = default;

	NotifyIconPopup& operator=(NotifyIconPopup const&) = default;

	NotifyIconPopup& operator=(NotifyIconPopup&&) = default;

	~NotifyIconPopup(void)
	{
	}
public:
	void PopupNotifierCustom(const char* msg, bool close = false);
	void Initialize();
	void EnableToastNotifications();

	HWND hwndWndProc;
private:
	HWND hwnd;
	std::string szCmdline;
	LPSTR g_lpCmdline;
	NOTIFYICONDATA NotifyIconSetting;
	BOOL Windowsmodification;
};
