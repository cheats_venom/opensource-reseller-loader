
#include <windows.h>
#include <tchar.h>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <Shlwapi.h>
#include "NotifyIconPop.h"
#include "fileutils/fileutils.h"
#include "Log/Log.h"
#include "Image/Icon.h"

#define TRAYICON_ID 100
#define WM_NICB WM_USER+100


void NotifyIconPopup::PopupNotifierCustom(const char* msg, bool close) {
	Log::Log(msg);
	NotifyIconSetting.cbSize = sizeof(NOTIFYICONDATA);
	NotifyIconSetting.hWnd = hwndWndProc;
	NotifyIconSetting.uID = TRAYICON_ID;
	NotifyIconSetting.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	NotifyIconSetting.uCallbackMessage = WM_NICB;
	NotifyIconSetting.hIcon =
		CreateIconFromResourceEx(Icon2, sizeof(Icon2), TRUE, 0x30000, 300, 300, LR_DEFAULTCOLOR); //Your icon here (.ico), use HxD to get Bytes

	Shell_NotifyIconA(NIM_ADD, &NotifyIconSetting);
	NotifyIconSetting.uFlags = NIF_INFO;

	auto split = [&](const std::string& s, char delim) -> std::vector<std::string> {
		std::vector<std::string> elems;
		std::stringstream ss(s);
		std::string item;
		while (std::getline(ss, item, delim)) {
			elems.push_back(item);
		}

		return elems;
	};

	std::vector<std::string> vCmdlineParts = split(szCmdline, ' ');
	int i = 0;
	std::string szNotificationTitle = ("Chair"); //Notification title
	std::string szNotificationContent = msg;
	int iTimeout = 5000; // 5 seconds
	for (std::string sz : vCmdlineParts) {
		switch (i++) {
		case 0:
			szNotificationTitle = sz;
			break;
		case 1:
			szNotificationContent = sz;
			break;
		case 2:
			iTimeout = std::stoi(sz);
			break;
		}
	}
	
	strcpy_s(NotifyIconSetting.szInfoTitle, szNotificationTitle.c_str());
	strcpy_s(NotifyIconSetting.szInfo, szNotificationContent.c_str());
	Shell_NotifyIconA(NIM_MODIFY, &NotifyIconSetting);

	if (close)
	{
		fileutils::TerminateLoader(); //If someone hooks this function, we will have a function next to leak free memory (Mymemset)
	}
}

LRESULT CALLBACK WndProc2(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
	case WM_CREATE:
		NotifyIconPopup::Get().hwndWndProc = hwnd;
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

void NotifyIconPopup::Initialize()
{
	WNDCLASSEX wc;
	MSG msg;
	std::string ClassName = ("NOTIFYICON");
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc2;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = NULL;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = ClassName.c_str();
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wc)) {
		fileutils::TerminateLoader(); //If someone hooks this function, we will have a function next to leak free memory (Mymemset)
	}

	hwnd = CreateWindowEx(0, ClassName.c_str(), "",
		0, 0, 0, 0, 0,
		NULL, NULL, NULL, NULL);

	if (hwnd == NULL) {
		fileutils::TerminateLoader(); //If someone hooks this function, we will have a function next to leak free memory (Mymemset)
	}
}

void NotifyIconPopup::EnableToastNotifications()
{
	HKEY hKey;
	DWORD data; DWORD size = sizeof(data); DWORD type = REG_DWORD;
	std::string ToastEnabled = ("ToastEnabled");

	auto EnableNotifications = [&](HKEY hKey) -> void
	{
		DWORD data = 1;
		RegSetValueExA(
			hKey,
			ToastEnabled.c_str(),
			0,
			REG_DWORD,
			reinterpret_cast<BYTE*>(&data),
			sizeof(data));

		fileutils::CustomShell((LPSTR)("powershell.exe"), (LPSTR)("/C Get-Service -Name WpnUserService* | Restart-Service -Force"), true, false);
	};

	if (NT_SUCCESS(RegOpenKeyExA(HKEY_CURRENT_USER, ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\PushNotifications"), NULL, KEY_ALL_ACCESS, &hKey)))
	{
		RegQueryValueExA(hKey, ToastEnabled.c_str(), NULL, &type, (LPBYTE)&data, &size);
		{
			if (data == 0)
			{
				EnableNotifications(hKey);
				RegCloseKey(hKey);
			}

			if (data < 0 || data > 1)
			{
				if (NT_SUCCESS(RegSetValueExA(hKey, ToastEnabled.c_str(), 0, REG_DWORD, (const BYTE*)data, sizeof DWORD)))
				{
					EnableNotifications(hKey);
					RegCloseKey(hKey);
				}
			}
		}		
	}
	else
	{
		fileutils::MessageBox(NULL, ("Error Code #4567"), ("Info"), MB_SYSTEMMODAL | MB_OK | MB_ICONASTERISK, NULL, 2000);
		fileutils::TerminateLoader();
	}
}
