#include "windows.h"
#include <string>

namespace Regedit {
	extern LSTATUS SetVal(std::string Key, std::string lpValue);
	extern LSTATUS SetVal(std::string Key, int lpValue);
	extern LSTATUS GetVal(std::string lpValue, int& out);
	extern LSTATUS GetVal(std::string lpValue, char* szString);
	extern LSTATUS GetVal(std::string lpValue, char* szString, size_t size);
	extern HKEY OpenKey(HKEY hRootKey, const char* strKey);
	extern bool DeleteKey(std::string Key);
	extern std::string GetDefaultPath();
}