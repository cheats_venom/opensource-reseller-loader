#include "Language.h"
#include "Regedit/Regedit.h"
#include "fileutils/fileutils.h"

std::map<std::string, Languages> Language::GetAvailableLanguages() {
	static const std::map<std::string, Languages> available_languages = {
		{"pt-BR", Languages::pt},
		{"pt-PT", Languages::pt},
		{"en-US", Languages::en},
	};
	return available_languages;
}

bool Language::ValidateLang(std::string lang) {
	auto available = GetAvailableLanguages();
	return available.find(lang) != available.end();
}

void Language::SetCurrentLanguage(std::string lang) {
	Regedit::SetVal("Language", lang.c_str());
}

Languages Language::GetCurrentLanguage() {
	{
		char tmp[255];
		if (Regedit::GetVal(("Language"), tmp) == ERROR_SUCCESS) {
			if (ValidateLang(tmp))
				return GetAvailableLanguages()[tmp];
		}
	}
	std::string Language = fileutils::GetUser_DefaultLocaleName();
	SetCurrentLanguage(Language);
	return GetAvailableLanguages()[Language];
}