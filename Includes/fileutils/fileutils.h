#include <Windows.h>
#include <string>
#include <vector>
#include <filesystem>

namespace fs = std::filesystem;

#define STATUS_SUCCESS ((NTSTATUS)0x00000000L)
#define STATUS_INTERNAL_ERROR ((NTSTATUS)0xC00000E5)
#define NT_SUCCESS(Status) (((NTSTATUS)(Status)) >= 0)

namespace fileutils
{
	std::vector<std::string> explode(std::string const &s, char delim);
	VOID AllocConsoleA();
	bool FileExists(const std::string &);
	std::string CurrentPath();
	std::vector<std::string> EnumerateFiles(std::string folder);
	std::string RandomString(std::size_t length);
	void CreateDir(std::string Path, bool isFile);
	bool DirExists(const std::string &dirName_in);
	int MessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType, WORD wLanguageId, DWORD dwMilliseconds);
	DWORD SetFileAttribute(std::string);
	std::string GetTempFolder();
	BOOL pWriteFile(std::string, void *, size_t);
	VOID TerminateLoader();
	BOOL TerminateProcessEx(DWORD dwProcessId);
	BOOL CustomShell(std::string Path, std::string command, bool wait, bool hide);
	std::string GetUser_DefaultLocaleName();
	std::string ReplaceAll(std::string str, const std::string &from, const std::string &to);
	std::wstring ReplaceAll(std::wstring str, const std::wstring& from, const std::wstring& to);
	BOOL CreateProcessA(std::string Path, bool hide);
	BOOL CreateProcessA(std::string Path, std::string command);
	BOOL AdjustCurrentPrivilege(LPCSTR privilege);
	bool StrContains(std::string s1, std::string s2);
	ULONG GenRandomCode(ULONG min, ULONG max);
	std::string convert_with_url(const std::wstring &str);
	std::string GetRegister();
	std::string GetDir();
}
