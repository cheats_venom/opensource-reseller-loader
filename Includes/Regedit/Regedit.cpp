#include "Regedit.h"
#include <memory>
#include <list>
#include <string>
#include <fileutils/fileutils.h>

namespace Regedit {
	std::string DefPath = std::string("SOFTWARE\\") + fileutils::GetRegister();

	std::string GetDefaultPath() {
		return DefPath;
	}

	LSTATUS SetVal(std::string Key, std::string lpValue) {
		HKEY hkey = OpenKey(HKEY_CURRENT_USER, DefPath.c_str());
		LSTATUS GetSetKey = RegSetValueExA(hkey, Key.c_str(), 0, REG_SZ, (BYTE*)lpValue.c_str(), lpValue.size());
		RegCloseKey(hkey);
		return GetSetKey;
	}

	LSTATUS SetVal(std::string Key, int lpValue) {
		HKEY hkey = OpenKey(HKEY_CURRENT_USER, DefPath.c_str());
		LSTATUS GetSetKey = RegSetValueExA(hkey, Key.c_str(), 0, REG_DWORD, (const BYTE*)&lpValue, sizeof(int));
		RegCloseKey(hkey);
		return GetSetKey;
	}

	LSTATUS GetVal(std::string lpValue, int& out) {
		DWORD dwType = REG_DWORD;
		DWORD dwSize = sizeof(int);
		HKEY hkey = OpenKey(HKEY_CURRENT_USER, DefPath.c_str());
		LSTATUS GetRegKey = RegGetValueA(hkey, NULL, lpValue.c_str(), RRF_RT_REG_DWORD, &dwType, &out, &dwSize);
		RegCloseKey(hkey);
		return GetRegKey;
	}

	LSTATUS GetVal(std::string lpValue, char* szString, size_t size) {
		DWORD dwType = REG_SZ;
		DWORD dwSize = size;
		HKEY hkey= OpenKey(HKEY_CURRENT_USER, DefPath.c_str());
		LSTATUS GetRegKey = RegGetValueA(hkey, NULL, lpValue.c_str(), RRF_RT_REG_SZ, &dwType, szString, &dwSize);
		RegCloseKey(hkey);
		return GetRegKey;
	}

	LSTATUS GetVal(std::string lpValue, char* szString) {
		return GetVal(lpValue, szString, 255);
	}

	HKEY OpenKey(HKEY hRootKey, const char* strKey) {
		HKEY hKey;
		LONG nError = RegOpenKeyEx(hRootKey, strKey, NULL, KEY_ALL_ACCESS, &hKey);

		if (nError == ERROR_FILE_NOT_FOUND) {
			nError = RegCreateKeyEx(hRootKey, strKey, NULL, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
		}
		return hKey;
	}

	bool DeleteKey(std::string Key) {

		HKEY hKey = OpenKey(HKEY_CURRENT_USER, DefPath.c_str());
		if (RegDeleteValueA(hKey, Key.c_str()) != ERROR_SUCCESS)
			return false;

		return true;
	}
}

