
#include <windows.h>
#include <fstream>
#include <TlHelp32.h>
#include "Language/Language.h"
#include "Va_string/Variadicstring.h"
#include "Regedit/Regedit.h"
#include "LoaderThreads/LoaderThreads.h"
#include "NotifyPopup/NotifyIconPop.h"
#include "fileutils/fileutils.h"
#include "Menu/Menu.h"

using namespace std::chrono_literals;

//Memory Enumeration
#pragma comment(lib, "Iphlpapi.lib")

//cpprest
#pragma comment(lib, "brotlicommon-static.lib")
#pragma comment(lib, "brotlidec-static.lib")
#pragma comment(lib, "brotlienc-static.lib")
#pragma comment(lib, "cpprest_2_10.lib")
#pragma comment(lib, "zlib.lib")
#pragma comment(lib, "crypt32.lib")
#pragma comment(lib, "bcrypt.lib")
#pragma comment(lib, "winhttp.lib")
#pragma comment(lib, "dwmapi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxgi.lib")

//tpm check
#pragma comment(lib, "Tbs.lib")

//timeGetTime
#pragma comment(lib, "Winmm.lib")

int CALLBACK WinMain(
	HINSTANCE   hInstance,
	HINSTANCE   hPrevInstance,
	LPSTR       lpCmdLine,
	int         nCmdShow
)
{
	Menu::Get().instance = hInstance;

	//Create Regedit Path
	Regedit::OpenKey(HKEY_LOCAL_MACHINE, Regedit::GetDefaultPath().c_str());

	//Setup Default Language
	Language::Get().GetCurrentLanguage();

	//Initialize toast notifications
	NotifyIconPopup::Get().EnableToastNotifications();
	NotifyIconPopup::Get().Initialize();

	//Start Threads
	LoaderThreads::Start();

	return 0;
}