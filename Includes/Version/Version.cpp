#include <fstream>
#include "Version.h"

#include <iostream>
#include <fstream>

#include <Web/WebRequest.h>
#include <Language/Language.h>

#include <fileutils/fileutils.h>

#include <Log/Log.h>
#include <Va_string/Variadicstring.h>
#include <LoaderThreads/LoaderThreads.h>


void DeleteFiles() {
	//Delete Old Loader
	std::ifstream ifs(va_string(("%s/%s"), fileutils::CurrentPath().c_str(), std::string("oldloader.txt")).c_str());
	if (!ifs.fail()) {
		std::string str;
		while (std::getline(ifs, str)) {
			remove(str.c_str());
		}
		ifs.close();
		remove(va_string(("%s/%s"), fileutils::CurrentPath().c_str(), std::string("oldloader.txt")).c_str());
	}
}

bool Version::Check() {
	Sleep(1000);
	DeleteFiles();
	std::string version = WebRequest::Get().GetRequest("https://pastebin.com/raw/33h8hGUs"); // Your server loader version
	if (!fileutils::StrContains(version, "1.0")) {
		std::string loaderName = va_string("%s.exe", fileutils::RandomString(10).c_str());
		bool down = WebRequest::Get().Download(std::string("https://mywebsite.com/loader.exe"), va_string(("%s/%s"), fileutils::CurrentPath().c_str(), loaderName.c_str()).c_str()); //Your loader download link
		if (!down) {
			fileutils::MessageBox(NULL, ("Failed to download new version!"), ("Info"), MB_SYSTEMMODAL | MB_OK | MB_ICONASTERISK, NULL, 5000);
			return false;
		}

		std::ofstream myfile(va_string(("%s/%s"), fileutils::CurrentPath().c_str(), std::string(("oldloader.txt"))).c_str());
		char myexepath[MAX_PATH] = { 0 };
		GetModuleFileNameA(NULL, myexepath, MAX_PATH);
		myfile << myexepath;
		myfile.close();
		if (!fileutils::CreateProcessA((LPSTR)va_string(("%s/%s"), fileutils::CurrentPath().c_str(), loaderName.c_str()).c_str(), false)) {
			fileutils::MessageBox(NULL, va_string(std::string(("Failed to open new version: " "%s/%s")), fileutils::CurrentPath().c_str(), loaderName.c_str()).c_str(), ("Info"), MB_SYSTEMMODAL | MB_OK | MB_ICONASTERISK, NULL, 5000);
		}
		fileutils::TerminateLoader();
		return false;
	}
	return true;
}

