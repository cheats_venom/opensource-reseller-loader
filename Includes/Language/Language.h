#pragma once
#include <Windows.h>
#include <string>
#include <map>
#include "Singleton.h"

enum Languages : uint8_t {
	pt,
	en
};

class Language
	: public Singleton<Language>
{
	friend class Singleton<Language>;
	Language(void)
	{
	}

	Language(Language const&) = default;

	Language(Language&&) = default;

	Language& operator=(Language const&) = default;

	Language& operator=(Language&&) = default;

	~Language(void)
	{
	}

public:
	bool ValidateLang(std::string lang);
	std::map<std::string, Languages> GetAvailableLanguages();
	Languages GetCurrentLanguage();
	void SetCurrentLanguage(std::string lang);
};