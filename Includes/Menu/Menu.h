#pragma once
#include <windows.h>
#include <Singleton.h>
#include <string>
#include <vector>

class Menu
	: public Singleton<Menu>
{
	friend class Singleton<Menu>;
	Menu(void)
	{
	}

	Menu(Menu const&) = default;

	Menu(Menu&&) = default;


	Menu& operator=(Menu const&) = default;

	Menu& operator=(Menu&&) = default;

	~Menu(void)
	{
	}
public:

	bool Start();
	void Loop();
	HINSTANCE instance = NULL;
	bool Loading = false;
	bool isError = true;
	bool hasSpoofer = false;
	std::string message = "";
	std::string key = "";
	std::string time_left = "";
	std::string time_left2 = "";
	std::string cheat_name = "";
	std::string store_name = "";
	std::string store_logo = "";
	int store_menu_color = 0x1A5FA0;
	std::string lock_loader_token = "";
	int page = 0;


	bool is_team = false;
	struct CheatList {
		int id;
		std::string name;
		bool hasSpofer;
		bool active;
	};
	std::vector<CheatList> available_cheats;
};