#include "WebRequest.h"
#include <windows.h>
#include <iostream>
#include <cpprest/http_client.h>
#include <cpprest/details/http_helpers.h>
#include <cpprest/details/basic_types.h>
#include <fileutils/fileutils.h>

struct MemoryStruct {
	char* memory;
	size_t size;
};

	size_t callbackfunction(void* ptr, size_t size, size_t nmemb, void* userdata)
	{
		FILE* stream = (FILE*)userdata;
		if (!stream)
		{
			return 0;
		}

		size_t written = fwrite((FILE*)ptr, size, nmemb, stream);
		return written;
	}
	size_t WriteMemoryCallback(void* contents, size_t size, size_t nmemb, void* userp)
	{
		size_t realsize = size * nmemb;
		struct MemoryStruct* mem = (struct MemoryStruct*)userp;

		void* ptr = realloc(mem->memory, mem->size + realsize + 1);
		if (ptr == NULL) {
			return 0;
		}

		mem->memory = (char*)ptr;
		memcpy(&(mem->memory[mem->size]), contents, realsize);
		mem->size += realsize;
		mem->memory[mem->size] = 0;

		return realsize;
	}

	size_t WriteCallbackString(void* contents, size_t size, size_t nmemb, void* userp)
	{
		((std::string*)userp)->append((char*)contents, size * nmemb);
		return size * nmemb;
	}

	int progress_func(void* ptr, double TotalToDownload, double NowDownloaded, double TotalToUpload, double NowUploaded)
	{
		if (TotalToDownload <= 0.0) {
			return 0;
		}
		
		return 0;
	}

	std::string WebRequest::GetRequest(std::string url) {
		bool isError = false;
		std::string returnstr;
		std::wstring url_(url.begin(), url.end());
		auto replaceDomain = [&]() {
			auto url2_ = fileutils::ReplaceAll(url_, L"chairfbi.com", L"chairfbi2.com");
			if (url_ != url2_) {
				url_ = url2_;
				return true;
			}
			return false;
			};
		auto makeRequest = [&](std::wstring& url) {
			isError = false;
			web::http::client::http_client client(url_);
			client.request(web::http::methods::GET, U("/"))
				.then([](const web::http::http_response& response) {
				return response.extract_utf8string(true);
					})
				.then([&returnstr, &isError](const pplx::task<std::string>& task) {
						try {
							returnstr = task.get();
						}
						catch (const web::http::http_exception& e) {
							isError = true;
						}
					})
						.wait();
			};
		try {
			makeRequest(url_);
		}
		catch (const web::http::http_exception& e) {
			isError = true;
		}


		if (isError && replaceDomain()) {
			try {
				makeRequest(url_);
			}
			catch (const web::http::http_exception& e) {
				isError = true;
			}
		}

		if (isError)
			return "Error";
		else {
			return returnstr;
		}
	}

	std::wstring WebRequest::GetWideRequest(std::string url) {
		bool isError = false;
		std::wstring returnw;
		std::wstring url_(url.begin(), url.end());
		auto replaceDomain = [&]() {
			auto url2_ = fileutils::ReplaceAll(url_, L"chairfbi.com", L"chairfbi2.com");
			if (url_ != url2_) {
				url_ = url2_;
				return true;
			}
			return false;
			};

		auto makeRequest = [&](std::wstring& url) {
			isError = false;
			web::http::client::http_client client(url_);
			client.request(web::http::methods::GET, U("/"))
				.then([](const web::http::http_response& response) {
				return response.extract_utf16string(true);
					})
				.then([&returnw, &isError](const pplx::task<std::wstring>& task) {
						try {
							returnw = task.get();
						}
						catch (const web::http::http_exception& e) {
							isError = true;
						}
					})
						.wait();
			};
		try {
			makeRequest(url_);
		}
		catch (const web::http::http_exception& e) {
			isError = true;
		}


		if (isError && replaceDomain()) {
			try {
				makeRequest(url_);
			}
			catch (const web::http::http_exception& e) {
				isError = true;
			}
		}

		if (isError)
			return L"Error";
		else {
			return returnw;
		}
	}

	bool WebRequest::Download(std::string url, std::string dir) {
		auto fileStream = std::make_shared<concurrency::streams::ostream>();
		bool isError = false;
		std::wstring filew(dir.begin(), dir.end());
		std::wstring url_(url.begin(), url.end());
		auto replaceDomain = [&]() {
			auto url2_ = fileutils::ReplaceAll(url_, L"chairfbi.com", L"chairfbi2.com");
			if (url_ != url2_) {
				url_ = url2_;
				return true;
			}
			return false;
			};

		auto makeRequest = [&](std::wstring& url) {
			isError = false;
			pplx::task<void> requestTask = concurrency::streams::fstream::open_ostream(filew).then([=](concurrency::streams::ostream outFile)
				{
					*fileStream = outFile;
					web::http::client::http_client client(url_);
					return client.request(web::http::methods::methods::GET);
				})
				.then([=](web::http::http_response response)
					{
						return response.body().read_to_end(fileStream->streambuf());
					})
					.then([=](size_t)
						{
							return fileStream->close();
						});
					requestTask.wait();
			};

		try
		{
			makeRequest(url_);
			return true;
		}
		catch (const std::exception& e)
		{
			isError = true;
		}


		if (isError && replaceDomain()) {
			try {
				makeRequest(url_);
				return true;
			}
			catch (const web::http::http_exception& e) {
				return false;
			}
		}
	}

	std::string WebRequest::PostJson(std::string url, web::json::value data) {
		bool isError = false;
		web::json::value returnJson;
		std::wstring url_(url.begin(), url.end());

		auto replaceDomain = [&]() {
			auto url2_ = fileutils::ReplaceAll(url_, L"chairfbi.com", L"chairfbi2.com");
			if (url_ != url2_) {
				url_ = url2_;
				return true;
			}
			return false;
			};

		auto makeRequest = [&](std::wstring& url) {
			isError = false;
			web::http::client::http_client client(url_);

			client.request(web::http::methods::POST, U("/"), data)
				.then([](const web::http::http_response& response) {
				return response.extract_json();
					})
				.then([&](const pplx::task<web::json::value>& task) {
						try {
							returnJson = task.get();
						}
						catch (const web::http::http_exception& e) {
							isError = true;
						}
					})
						.wait();
			};

		try {
			makeRequest(url_);
		}
		catch (const web::http::http_exception& e) {
			isError = true;
		}


		if (isError && replaceDomain()) {
			try {
				makeRequest(url_);
			}
			catch (const web::http::http_exception& e) {
				isError = true;
			}
		}


		if (isError)
			return "Error";
		else {
			std::wstring retw(returnJson.serialize().c_str());
			std::string rets(retw.begin(), retw.end());
			return rets;
		}
	}