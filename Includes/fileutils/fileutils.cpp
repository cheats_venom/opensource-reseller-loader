#define WIN32_LEAN_AND_MEAN
#include "fileutils.h"
#include <io.h>
#define access _access_s
#include <stdio.h>
#include <fstream>
#include <Psapi.h>
#include <TlHelp32.h>
#include <comdef.h>
#include <shellapi.h>
#include <ctime>
#include <direct.h>
#include <random>
#include <direct.h>
#include <iostream>
#include <istream>
#include <sstream>
#include "NotifyPopup/NotifyIconPop.h"
#include "LoaderThreads/LoaderThreads.h"
#include "Va_string/Variadicstring.h"
#include "Log/Log.h"
#include "Language/Language.h"
#include <codecvt>

std::vector<std::string> fileutils::explode(std::string const &s, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(s);

	for (std::string token; std::getline(iss, token, delim);)
	{
		result.push_back(std::move(token));
	}
	return result;
}

VOID fileutils::AllocConsoleA()
{
	AllocConsole();
	FILE *ssttree;
	freopen_s(&ssttree, ("CONIN$"), ("r"), stdin);
	freopen_s(&ssttree, ("CONOUT$"), ("w"), stdout);
	freopen_s(&ssttree, ("CONOUT$"), ("w"), stderr);
}

bool fileutils::FileExists(const std::string &Filename)
{
	return access(Filename.c_str(), 0) == 0;
}

std::string fileutils::CurrentPath()
{
	char buffer[MAX_PATH];
	GetModuleFileNameA(NULL, buffer, MAX_PATH);
	std::string::size_type pos = std::string(buffer).find_last_of("\\/");
	return std::string(buffer).substr(0, pos);
}

std::vector<std::string> fileutils::EnumerateFiles(std::string folder)
{
	std::vector<std::string> names;
	std::string search_path = folder + "/*.*";
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				names.push_back(fd.cFileName);
			}
		} while (::FindNextFile(hFind, &fd));
		::FindClose(hFind);
	}
	return names;
}

std::string fileutils::RandomString(std::size_t length)
{
	const std::string CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	std::random_device random_device;
	std::mt19937 generator(random_device());
	std::uniform_int_distribution<> distribution(0, CHARACTERS.size() - 1);

	std::string random_string;

	for (std::size_t i = 0; i < length; ++i)
	{
		random_string += CHARACTERS[distribution(generator)];
	}

	return random_string;
}

void fileutils::CreateDir(std::string Path, bool isFile)
{
	char opath[MAX_PATH];
	char *p;
	size_t len;

	if (isFile)
	{
		if (Path.find_last_of("/\\") != std::string::npos)
			Path = Path.substr(0, Path.find_last_of("/\\"));
		else
			return;
	}

	strncpy_s(opath, Path.c_str(), sizeof(opath));
	len = strlen(opath);

	if (opath[len - 1] == L'/')
	{
		opath[len - 1] = L'\0';
	}

	for (p = opath; *p; p++)
	{
		if (*p == L'/' || *p == L'\\')
		{
			*p = L'\0';

			if (_access(opath, 0))
			{
				_mkdir(opath);
			}

			*p = L'\\';
		}
	}

	if (_access(opath, 0))
	{
		_mkdir(opath);
	}
}

bool fileutils::DirExists(const std::string &dirName_in)
{
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;

	return false;
}

int fileutils::MessageBox(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType, WORD wLanguageId, DWORD dwMilliseconds)
{
	HMODULE hUser32 = LoadLibrary(("user32.dll"));
	int result = 0;
	if (hUser32)
	{

		typedef int(__stdcall * MSGBOXAAPI)(IN HWND hWnd,
																				IN LPCSTR lpText, IN LPCSTR lpCaption,
																				IN UINT uType, IN WORD wLanguageId, IN DWORD dwMilliseconds);

		auto MMessageBox = [&](HWND hWnd, LPCSTR lpText,
													 LPCSTR lpCaption, UINT uType, WORD wLanguageId,
													 DWORD dwMilliseconds) -> int
		{
			static MSGBOXAAPI MsgBoxTOA = NULL;

			if (!MsgBoxTOA)
			{
				HMODULE hUser32 = GetModuleHandle(("user32.dll"));
				if (hUser32)
				{
					MsgBoxTOA = (MSGBOXAAPI)GetProcAddress(hUser32,
																								 ("MessageBoxTimeoutA"));
				}
				else
				{
					FreeLibrary(hUser32);
					return 0;
				}
			}

			if (MsgBoxTOA)
			{
				return MsgBoxTOA(hWnd, lpText, lpCaption,
												 uType, wLanguageId, dwMilliseconds);
			}
		};

		result = MMessageBox(hWnd, lpText, lpCaption, uType, wLanguageId, dwMilliseconds);
		FreeLibrary(hUser32);
	}
	return result;
}

DWORD fileutils::SetFileAttribute(std::string File)
{

	DWORD attr = GetFileAttributes(File.c_str());
	if (attr)
	{
		SetFileAttributesA(File.c_str(), FILE_ATTRIBUTE_SYSTEM | FILE_ATTRIBUTE_HIDDEN);
	}
	return attr;
}

std::string fileutils::GetTempFolder()
{
	std::string TempPath;
	char wcharPath[MAX_PATH];
	if (GetTempPath(MAX_PATH, wcharPath))
		TempPath = wcharPath;

	return TempPath;
}

BOOL fileutils::pWriteFile(std::string filename, void *FileDump, size_t Size)
{
	PVOID Data = reinterpret_cast<char *>(FileDump);

	if (!FileExists(filename))
	{
		FILE *pFile;
		errno_t file = fopen_s(&pFile, filename.c_str(), "wb");
		fwrite(Data, 1, Size, pFile);
		fclose(pFile);

		return true;
	}

	return false;
}

VOID fileutils::TerminateLoader()
{
	ExitProcess(0);
}

BOOL fileutils::TerminateProcessEx(DWORD dwProcessId)
{
	HANDLE hProc = OpenProcess(PROCESS_TERMINATE, FALSE, dwProcessId);
	BOOL result = TerminateProcess(hProc, 0x100);
	if (result)
	{
		CloseHandle(hProc);
		return result;
	}
	return FALSE;
}

BOOL fileutils::CustomShell(std::string Path, std::string command, bool wait, bool hide)
{
	SHELLEXECUTEINFO lpExecInfo;
	DWORD dwExitCode;
	HINSTANCE hProcess = 0;
	BOOL bResult;

	ZeroMemory(&lpExecInfo, sizeof(lpExecInfo));
	lpExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	lpExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	lpExecInfo.hwnd = NULL;
	lpExecInfo.lpVerb = ("runas");
	lpExecInfo.lpFile = (LPSTR)Path.c_str();
	lpExecInfo.lpParameters = (LPSTR)command.c_str();
	lpExecInfo.lpDirectory = ("");
	lpExecInfo.nShow = hide ? SW_HIDE : SW_SHOWNORMAL;
	lpExecInfo.hInstApp = NULL;
	lpExecInfo.hProcess = hProcess;

	bResult = ShellExecuteEx(&lpExecInfo);

	if (bResult)
	{
		if (wait)
			WaitForSingleObject(lpExecInfo.hProcess, INFINITE);
		CloseHandle(lpExecInfo.hProcess);
	}

	return bResult;
}

std::string fileutils::GetUser_DefaultLocaleName()
{
	wchar_t localeName[LOCALE_NAME_MAX_LENGTH] = {0};
	if (!GetUserDefaultLocaleName(localeName, ARRAYSIZE(localeName)))
	{
		goto finish;
	}
	{
		size_t origsize = wcslen(localeName) + 1;
		char nstring[255];
		wcstombs_s(NULL, nstring, origsize, localeName, _TRUNCATE);
		if (Language::Get().ValidateLang(nstring))
			return nstring;
	}
finish:
	return Language::Get().GetAvailableLanguages().begin()->first;
}

std::string fileutils::ReplaceAll(std::string str, const std::string &from, const std::string &to)
{
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos)
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
	}
	return str;
}

std::wstring fileutils::ReplaceAll(std::wstring str, const std::wstring& from, const std::wstring& to)
{
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::wstring::npos)
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // Lida com o caso onde 'to' � um substring de 'from'
	}
	return str;
}

BOOL fileutils::AdjustCurrentPrivilege(LPCSTR privilege)
{
	LUID luid = {0};
	if (!LookupPrivilegeValue(0, privilege, &luid))
	{
		Log::Log("[-]Cannot adjust permissions to %s  %d \n", privilege, GetLastError());
		return FALSE;
	}

	TOKEN_PRIVILEGES tp = {0};
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	HANDLE token = 0;
	if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &token))
	{
		Log::Log("[-] Cannot open the current process ID  %d \n", GetLastError());
		return FALSE;
	}

	if (!AdjustTokenPrivileges(token, FALSE, &tp, sizeof(tp), 0, 0))
	{
		Log::Log("[-] Cannot adjust the flag of the current process %d \n", GetLastError());
		CloseHandle(token);
		return FALSE;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)
	{
		Log::Log("[-] Permission adjustment failed\n");
		CloseHandle(token);
		return FALSE;
	}

	CloseHandle(token);
	return TRUE;
}

bool fileutils::StrContains(std::string s1, std::string s2)
{
	return s1.find(s2) != std::string::npos;
}

BOOL fileutils::CreateProcessA(std::string Path, std::string command)
{
	BOOL bResult;
	STARTUPINFO info = {sizeof(info)};
	PROCESS_INFORMATION processInfo;
	bResult = CreateProcessA((LPSTR)Path.c_str(), (LPSTR)command.c_str(), NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &info, &processInfo);

	if (bResult)
	{
		WaitForSingleObject(processInfo.hProcess, INFINITE);
		CloseHandle(processInfo.hProcess);
		CloseHandle(processInfo.hThread);
	}

	return bResult;
}

BOOL fileutils::CreateProcessA(std::string Path, bool hide)
{

	SHELLEXECUTEINFO shExInfo = {0};
	shExInfo.cbSize = sizeof(shExInfo);
	shExInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	shExInfo.hwnd = 0;
	shExInfo.lpVerb = ("runas");					 // Operation to perform
	shExInfo.lpFile = (LPSTR)Path.c_str(); // Application to start
	shExInfo.lpParameters = "";						 // Additional parameters
	shExInfo.lpDirectory = 0;
	shExInfo.nShow = hide ? SW_HIDE : SW_SHOW;
	shExInfo.hInstApp = 0;

	if (ShellExecuteExA(&shExInfo))
	{
		return true;
	}

	return false;
}

ULONG fileutils::GenRandomCode(ULONG min, ULONG max)
{
	std::random_device rand_dev;
	std::mt19937 generator(rand_dev());
	std::uniform_int_distribution<ULONG> distr(min, max);
	return distr(generator);
}

std::string urlDecode(const std::string &str)
{
	std::string result;
	for (size_t i = 0; i < str.size(); ++i)
	{
		if (str[i] == '%' && i + 2 < str.size() && isxdigit(str[i + 1]) && isxdigit(str[i + 2]))
		{
			int value;
			std::istringstream hex(str.substr(i + 1, 2));
			hex >> std::hex >> value;
			result += static_cast<char>(value);
			i += 2;
		}
		else
		{
			result += str[i];
		}
	}
	return result;
}

std::string fileutils::convert_with_url(const std::wstring &str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
	return urlDecode(converter.to_bytes(str));
}
std::string fileutils::GetDir()
{
	return std::string("C:\\") + GetRegister();
}

std::string fileutils::GetRegister()
{
	wchar_t username[MAX_PATH + 1];
	DWORD username_len = MAX_PATH + 1;
	GetUserNameW(username, &username_len);
	std::wstring wcsusername(username);
	if (wcsusername.length() <= 0)
	{
		wcsusername = L"Default";
	}
	std::string final_path;
	for (int i = 0; i < wcsusername.length(); i++)
	{
		final_path += std::to_string(static_cast<int>(wcsusername.at(i)));
	}
	if (final_path.length() > 10) {
		final_path = final_path.substr(0, 10);
	}
	return final_path + std::string("RSCFG");
}