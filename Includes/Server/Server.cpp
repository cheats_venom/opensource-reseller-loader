#include "Server.h" #include "User.h"
#include <Iphlpapi.h>
#include <tchar.h>
#include <atlstr.h>
#include <thread>
#include <map>
#include <writer.h>
#include <document.h>
#include "Web/WebRequest.h"
#include "Language/Language.h"
#include "Log/Log.h"
#include "Regedit/Regedit.h"
#include "fileutils/fileutils.h"
#include "Va_string/Variadicstring.h"
#include "Menu/Menu.h"
#include <LoaderThreads/LoaderThreads.h>
#include <direct.h>

void Server::Load()
{
		char tmp[255];
	if (Regedit::GetVal(("Spoofer"), tmp) == ERROR_SUCCESS)
		wantSpoofer = std::string(tmp) == ("true");

	if (Regedit::GetVal(("RKey"), tmp) == ERROR_SUCCESS)
		Menu::Get().key = std::string(tmp);

	if (Regedit::GetVal(("SpooferDiskType"), tmp) == ERROR_SUCCESS)
		spoofDiskType = std::string(tmp) == ("true");

	if (Regedit::GetVal(("SpooferDisk"), tmp) == ERROR_SUCCESS)
		spoofDisk = std::string(tmp) == ("true");

	if (Regedit::GetVal(("SpooferMobo"), tmp) == ERROR_SUCCESS)
		spoofMobo = std::string(tmp) == ("true");

	if (Regedit::GetVal(("SpooferBoot"), tmp) == ERROR_SUCCESS)
		spoofBoot = std::string(tmp) == ("true");

	if (Regedit::GetVal(("SpooferMAC"), tmp) == ERROR_SUCCESS)
		spoofMAC = std::string(tmp) == ("true");

	if (Regedit::GetVal(("SpooferMonitor"), tmp) == ERROR_SUCCESS)
		spoofMonitor = std::string(tmp) == ("true");

	if (Regedit::GetVal(("SpooferGPU"), tmp) == ERROR_SUCCESS)
		spoofGPU = std::string(tmp) == ("true");

	if (Regedit::GetVal(("SpooferTPM"), tmp) == ERROR_SUCCESS)
		spoofTPM = std::string(tmp) == ("true");

	if (Regedit::GetVal(("SpooferFileRegistry"), tmp) == ERROR_SUCCESS)
		spoofFileRegistry = std::string(tmp) == ("true");

	if (Regedit::GetVal(("spooferStaticSerial"), tmp) == ERROR_SUCCESS)
		spoofStaticSerial = std::string(tmp) == ("true");

}

void Server::Save()
{
		Regedit::SetVal(("RKey"), Menu::Get().key.c_str());
	Regedit::SetVal(("Spoofer"), wantSpoofer ? ("true") : ("false"));
	Regedit::SetVal(("SpooferDisk"), spoofDisk ? ("true") : ("false"));
	Regedit::SetVal(("SpooferDiskType"), spoofDiskType ? ("true") : ("false"));
	Regedit::SetVal(("SpooferMobo"), spoofMobo ? ("true") : ("false"));
	Regedit::SetVal(("SpooferBoot"), spoofBoot ? ("true") : ("false"));
	Regedit::SetVal(("SpooferMAC"), spoofMAC ? ("true") : ("false"));
	Regedit::SetVal(("SpooferMonitor"), spoofMonitor ? ("true") : ("false"));
	Regedit::SetVal(("SpooferGPU"), spoofGPU ? ("true") : ("false"));
	Regedit::SetVal(("SpooferTPM"), spoofTPM ? ("true") : ("false"));
	Regedit::SetVal(("SpooferFileRegistry"), spoofFileRegistry ? ("true") : ("false"));
	Regedit::SetVal(("spooferStaticSerial"), spoofStaticSerial ? ("true") : ("false"));
}

bool Server::LoginTeam(std::string key)
{
	Menu::Get().message = "";
	Menu::Get().Loading = true;
	Menu::Get().isError = false;

	auto res = fileutils::convert_with_url(WebRequest::Get().GetWideRequest(
		va_string("https://access.chairfbi.com/loader/team_check?key=%s&token=%s", key.c_str(), Menu::Get().lock_loader_token.c_str())));
	if (res == "Error")
		return false;

	rapidjson::Document resultJson;
	resultJson.Parse(res.c_str());
	if (resultJson.HasMember(("error")) && resultJson.HasMember(("code")))
	{
		return false;
	}
	else if (resultJson.HasMember("cheats") && resultJson[("cheats")].IsArray() && resultJson.HasMember(("store")) && resultJson[("store")].IsObject())
	{
		Save();

		auto storeJson = resultJson[("store")].GetObj();
		auto store_name = std::string(storeJson[("name")].GetString(), storeJson[("name")].GetStringLength());
		auto store_logo = std::string(storeJson[("logo")].GetString(), storeJson[("logo")].GetStringLength());
		auto store_menu_color = storeJson[("menu_color")].GetInt();
		Menu::Get().Loading = false;
		Menu::Get().isError = false;
		Menu::Get().store_name = store_name;
		Menu::Get().store_logo = store_logo;
		Menu::Get().store_menu_color = store_menu_color;
		Menu::Get().is_team = true;

		for (auto& v : resultJson[("cheats")].GetArray())
		{
			auto storeCheat = v.GetObj();
			auto cheat_id = storeCheat["id"].GetInt();
			auto name = std::string(storeCheat[("name")].GetString(), storeCheat[("name")].GetStringLength());
			auto include_spoofer = storeCheat["include_spoofer"].GetBool();
			auto active = storeCheat["active"].GetBool();
			Menu::Get().available_cheats.push_back({ cheat_id,
																							name,
																							include_spoofer,
																							active });
		}

		Menu::Get().hasSpoofer = Menu::Get().available_cheats[0].hasSpofer;
		if (!Menu::Get().available_cheats[0].active)
		{
			Menu::Get().isError = true;
			Menu::Get().message = std::string(("Cheat is under maintence"));
		}

		Menu::Get().page = 1;
		return true;
	}
}

void Server::LoginKey(std::string key)
{
	if (LoginTeam(key)) {
		return;
	}
	Menu::Get().message = "";
	Menu::Get().Loading = true;
	Menu::Get().isError = false;

	auto res = fileutils::convert_with_url(WebRequest::Get().GetWideRequest(va_string("https://access.chairfbi.com/loader/key_check?key=%s&token=%s", key.c_str(), Menu::Get().lock_loader_token.c_str())));
	if (res == "Error")
		return LoaderThreads::ErrorClose("Error not handled");

	rapidjson::Document resultJson;
	resultJson.Parse(res.c_str());

	if (resultJson.HasMember(("error")) && resultJson.HasMember(("code")))
	{
		auto code = std::string(resultJson[("code")].GetString(), resultJson[("code")].GetStringLength());
		if (code == "invalid_key")
		{
			return LoaderThreads::ErrorClose("Invalid key");
		}
		else if (code == "expired_key")
		{
			return LoaderThreads::ErrorClose("Key expired");
		}
		return LoaderThreads::ErrorClose("Error not handled");
	}
	else if (resultJson.HasMember("expires_in") && resultJson[("expires_in")].IsObject() && resultJson.HasMember("cheat") && resultJson[("cheat")].IsObject() && resultJson.HasMember(("store")) && resultJson[("store")].IsObject())
	{
		Save();

		auto storeJson = resultJson[("store")].GetObj();
		auto cheatJson = resultJson[("cheat")].GetObj();
		auto expireJson = resultJson[("expires_in")].GetObj();
		auto store_name = std::string(storeJson[("name")].GetString(), storeJson[("name")].GetStringLength());
		auto store_logo = std::string(storeJson[("logo")].GetString(), storeJson[("logo")].GetStringLength());
		auto store_menu_color = storeJson[("menu_color")].GetInt();

		auto cheat_name = std::string(cheatJson[("name")].GetString(), cheatJson[("name")].GetStringLength());
		auto hasSpoofer = cheatJson[("include_spoofer")].GetBool();
		auto active = cheatJson[("active")].GetBool();

		auto expire_days = expireJson[("day")].GetFloat();
		auto expire_hours = expireJson[("hour")].GetFloat();
		auto expire_minutes = expireJson[("minute")].GetFloat();

		Menu::Get().time_left = va_string("Expires in %.0f day(s), %.0f hour(s), %.0f minute(s)", expire_days, expire_hours, expire_minutes);
		Menu::Get().time_left2 = va_string("Expires in %.0f d %.0f h %.0f m", expire_days, expire_hours, expire_minutes);

		Menu::Get().Loading = false;
		Menu::Get().isError = false;
		Menu::Get().store_name = store_name;
		Menu::Get().store_logo = store_logo;
		Menu::Get().store_menu_color = store_menu_color;
		Menu::Get().cheat_name = cheat_name;
		Menu::Get().hasSpoofer = hasSpoofer;

		if (!active) {
			Menu::Get().Loading = true;
			Menu::Get().isError = true;
			Menu::Get().message = std::string(("Cheat is under maintence"));
		}
		Menu::Get().page = 1;
	}
}

void DeleteOldLoaders(std::string current_ver)
{
	auto all_files = fileutils::EnumerateFiles(va_string(("%s\\"), fileutils::GetDir().c_str()));
	for (auto file : all_files)
	{
		if (fileutils::StrContains(file, "laddon.exe") && !fileutils::StrContains(file, current_ver))
		{
			remove(va_string("%s\\%s", fileutils::GetDir().c_str(), file.c_str()).c_str());
		}
	}
}

std::string api_version = "1";
std::string DownloadLoader()
{
	std::time_t t = std::time(0); // get time now
	std::tm* now = std::localtime(&t);
	std::string version = WebRequest::Get().GetRequest(std::string("https://access.chairfbi.com/loader/version/NoUi") + api_version);
	_mkdir(fileutils::GetDir().c_str());
	std::string loader_name = va_string("%s\\%s-laddon.exe", fileutils::GetDir().c_str(), version.c_str());
	DeleteOldLoaders(va_string("%s-laddon.exe", version.c_str()));
	if (!fileutils::FileExists(loader_name))
	{
		std::string url = WebRequest::Get().GetRequest("https://access.chairfbi.com/loader/url/NoUi" + api_version);
		bool down = WebRequest::Get().Download(url, loader_name.c_str());
		if (!down)
		{
			Sleep(2000);
			remove(loader_name.c_str());
			LoaderThreads::ErrorClose("Failed to download loader addon");
			return "";
		}
	}
	return loader_name;
}

void Server::InjectKey(std::string key, int cheat_id)
{
		Menu::Get().message = "";
	Menu::Get().Loading = true;
	Menu::Get().isError = false;

	std::string spoofer_parameters = va_string("%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d",
		wantSpoofer,
		spoofDisk,
		spoofDiskType,
		spoofMobo,
		spoofBoot,
		spoofMAC,
		spoofMonitor,
		spoofGPU,
		spoofTPM,
		spoofFileRegistry,
		spoofStaticSerial,
		spoofStaticSerialRandomSeed
	);

	auto parameters = va_string("%s %d %s %d",
		key.c_str(),
		Menu::Get().store_menu_color,
		spoofer_parameters.c_str(),
		cheat_id
	);

	auto loader_name = DownloadLoader();
	if (loader_name.empty() || !fileutils::FileExists(loader_name))
	{
		return LoaderThreads::ErrorClose("Failed to download loader addon");
	}

	if (!fileutils::CustomShell((LPSTR)(loader_name.c_str()), (LPSTR)parameters.c_str(), true, true))
	{
		Sleep(2000);
		remove(loader_name.c_str());
		return LoaderThreads::ErrorClose("Failed to load loader addon");
	}

	Sleep(2000);

	auto InjResultFile = "Inj.log";

	if (!fileutils::FileExists(InjResultFile))
	{
		return LoaderThreads::ErrorClose("Inj File Error");
	}

	std::ifstream ifs(InjResultFile);
	std::string inj_result((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));
	ifs.close();
	remove(InjResultFile);
	remove(loader_name.c_str());
	auto error_message = std::string("Unknown Error");
	bool is_error = true;

	if (!inj_result.empty())
		error_message = inj_result;

	if (fileutils::StrContains(inj_result, "success")) {
		error_message = "Cheat injected!";
		is_error = false;
	}

	if (fileutils::StrContains(inj_result, "injection failed"))
		error_message = "An error occurred while injecting, restart your PC and try again!";

	if (fileutils::StrContains(inj_result, "internal error"))
		error_message = "Server Error";

	if (fileutils::StrContains(inj_result, "hwid mismatch"))
		error_message = "HWID is not the same registered";

	if (fileutils::StrContains(inj_result, "key invalid"))
		error_message = "Invalid Key";

	if (fileutils::StrContains(inj_result, "unknown error"))
		error_message = "Unknown Error";

	if (fileutils::StrContains(inj_result, "wrong requirements"))
		error_message = "Your PC doesn't meet the minimum requirements!";

	if (fileutils::StrContains(inj_result, "version outdated"))
		error_message = "Failed to load loader addon";

	if (fileutils::StrContains(inj_result, "process must be closed"))
		error_message = "The process must be closed to inject the cheat";

	if (fileutils::StrContains(inj_result, "process must be open"))
		error_message = "The process must be open to inject the cheat";

	if (fileutils::StrContains(inj_result, "under maintenance"))
		error_message = "The cheat is under maintenance";

	if (is_error)
		LoaderThreads::ErrorClose(error_message.c_str());
	else
		LoaderThreads::SuccessClose(error_message.c_str());
}