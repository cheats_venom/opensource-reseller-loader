#pragma once

namespace Log {
	extern void Log(const char* fmt, ...);
}